#!/bin/bash
if type gshuf > /dev/null; then
 cowfile="$(cowsay -ln | sed "1 d" | tr ' ' '\n' | gshuf -n 1)"
else
 cowfiles=( $(cowsay -ln | sed "1 d") );
 cowfile=${cowfiles[$(($RANDOM % ${#cowfiles[*]}))]}
fi
cowsay -nf "$cowfile"
